using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour
{
    [SerializeField] HouseData data;
    [SerializeField] int maxIterations = 8;
    [SerializeField] int cleanFrom = 5;
    HouseRenderer rend;
    HouseGenerator gen;
    HouseGrammar grammar;
    List<Wing> wingsToRender;

    void Start()
    {
        rend = GetComponent<HouseRenderer>();
        gen = new HouseGenerator();
        grammar = new HouseGrammar(maxIterations, cleanFrom);
        // grammar einen Satz produzieren lassen
        // house gen generieren lassen
        // house rend initialisieren
    }

    void Update()
    {
    }
}
