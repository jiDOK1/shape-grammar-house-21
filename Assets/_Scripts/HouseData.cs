using UnityEngine;

public class HouseData : ScriptableObject
{
    [Range(2f, 50f)] public float FrontLength = 6f;
    [Range(2f, 50f)] public float SideLength = 8f;
    [Range(2, 100)] public int NumFloors = 8;
    [Range(0f, 360f)] public float ExtraYRot;
    public int RandomIndicesLength = 8;
    public GameObject[] WallModules;
    public GameObject[] SlopeModules;
    public GameObject[] GableModules;
    public GameObject RoofCap;
}
