using System.Text;
using System.Collections.Generic;
using UnityEngine;

public class HouseGrammar
{
    Dictionary<char, string> rules = new Dictionary<char, string>();
    string axiom = "w";
    string sentence;
    char separator = '|';
    int maxIterations;
    int cleanFrom;

    public HouseGrammar(int maxIterations, int cleanFrom)
    {
        this.maxIterations = maxIterations;
        this.cleanFrom = cleanFrom;
    }

    public string Produce()
    {
        rules.Clear();
        AddRules();
        sentence = axiom;
        for (int i = 0; i < maxIterations; i++)
        {
            GenerateSentence(i >= cleanFrom);
        }
        Debug.Log(sentence);
        return sentence;
    }

    void AddRules()
    {
        rules['w'] = "W[ww]|W[FR]";
        rules['F'] = "F|FF";
    }

    void GenerateSentence(bool clean)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sentence.Length; i++)
        {
            char curSymbol = sentence[i];
            if(clean && curSymbol == 'w')
            {
                continue;
            }
            if (rules.ContainsKey(curSymbol))
            {
                string ruleString = rules[curSymbol];
                string[] symbols = ruleString.Split(separator);
                string cluster = symbols[Random.Range(0, symbols.Length)];
                sb.Append(cluster);
            }
            else
            {
                sb.Append(curSymbol);
            }
        }
        sentence = sb.ToString();
    }
}
